package linkedList;

import java.io.Serializable;

//Node in each of our linked list
//Generic should be an object
@SuppressWarnings("serial")
public class Node<T> implements Serializable{

	Node<T> next;
	T data;
	
	//Constructor
	public Node(T d){
		
		next=null;
		data=d;
	}
	
	//Constructor used if I want to specify the next node
	public Node(T d, Node<T> n){
		
		next=n;
		data=d;
	}
	
	//Returns the data
	public T getData(){
		
		return data;
	}
	
	//Sets the data value to d
	public void setData(T d){
	
		data=d;
	}
	
	//Returns the next node
	public Node<T> getNext(){
		
		return next;
	}
	
	//Sets the next node
	public void setNext(Node<T> n){
		
		next=n;
	}
}
