package linkedList;

import java.io.Serializable;

import src.Food;

//Linked list which will handle all data collection
/**
 * @author John
 *
 * @param <Object>
 */

@SuppressWarnings("hiding")
public class linkedList<Object> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Node<Object> head;
	private int count;
	
	/**
	 * @return linkedList
	 */
	//Constructor
	public linkedList(){
	
		head= new Node<Object>(null);
		count= 0;
	}
	
	/**
	 * @return count
	 */
	//Returns the count of how many items are in the linked list 
	public int getSize(){
		
		return count;
	}
	
	/**
	 * @param data
	 * @return void
	 */
	//Takes in data, then adds a Node<T> at the end of the list with the data
	public void add(Object data){
		
		Node<Object> temp= new Node<Object>(data);
		Node<Object> current= head;
		
		while(current.getNext() != null){
			
			current=current.getNext();
		}
		
		current.setNext(temp);
		count++;
	}
	
	/**
	 * @param data, index
	 * @return void
	 */
	//Takes in data, then sets the data in a Node<T> at the specific index. If the index is not found,
	//it instead adds it to the end of the list
	public void set(Object data, int index){
		
		Node<Object> temp= new Node<Object>(data);
		Node<Object> current= head;
		
		for(int i=0; i< index && current.getNext() != null; i++){
			
			current= current.getNext();
		}
		
		temp.setNext(current.getNext().getNext());
		current.setNext(temp);
		count++;
	}
	
	/**
	 * @param index
	 * @return Object
	 */
	//Returns the data in the Node<T> at the selected index
	public Object get(int index){
		
		//Checks to make sure the user isnt an idiot
		if(index<0){
			
			return null;
		}
		Node<Object> current= head.getNext();
		
		for(int i=0; i<index; i++){
			
			if(current.getNext() == null){
				
				return null;
			}
			current= current.getNext();
		}
		return current.getData();
	}
	/**
	 * removes target node at index
	 */
	public boolean remove(int index){
		
		//First makes sure the user isnt an idiot
		if(index<0 || index> getSize()){
			
			return false;
		}
		Node<Object> current= head; 
		for(int i=0;i<index;i++){
			
			if(current.getNext() == null){
				
				return false;
			}
			current= current.getNext();
		}
		current.setNext(current.getNext().getNext());
		count--;
		return true;
	}
}
