package interact;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import binaryTree.Tree;

//Takes in a binary file and then loads the information into an array
/**
 * @author John
 *
 */
public class loadFile {

	//Takes in an array of food and then writes it into a binary file
	/**
	 * @return categoryList
	 */
		@SuppressWarnings("unchecked")
		public Tree load(){
			
			String fileName= "C:\\Users\\jjh20130\\workspace\\out.bin";
			Tree c = null;
			
			try{
				
				FileInputStream fIs= new FileInputStream(fileName);
				ObjectInputStream os= new ObjectInputStream(fIs);
				
				c= (Tree) os.readObject();
				
				fIs.close();
				os.close();
			}catch(FileNotFoundException e){
				
				e.printStackTrace();
			}catch(IOException e){
				
				e.printStackTrace();
			}catch(ClassNotFoundException e){
				
				e.printStackTrace();
			}
			
			return c;
		}
}
