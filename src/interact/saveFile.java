package interact;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import binaryTree.Tree;

//Saves a list into a binary file
public class saveFile {

	//Takes in an array of food and then writes it into a binary file
	public void save(Tree tree){
		
		String fileName= "C:\\Users\\jjh20130\\workspace\\out.bin";
		
		try{
			
			FileOutputStream fOs= new FileOutputStream(fileName);
			ObjectOutputStream os= new ObjectOutputStream(fOs);
			
					os.writeObject(tree);
					os.close();
					fOs.close();
		}catch(FileNotFoundException e){
			
			e.printStackTrace();
		}catch(IOException e){
			
			e.printStackTrace();
		}
	}
}
