package binaryTree;

import java.io.Serializable;

import linkedList.linkedList;
import src.Food;

public class Tree implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Creates the root of the class
	leaf<Food> root;
	
	/**
	 * Constructor
	 */
	//Constructor
	public Tree(){
		
		root= null;
	}
	
	public leaf<Food> getRoot(){
		
		return root;
	}
	/**
	 * Sets root
	 *
	 * @param leaf l
	 * @return void
	 */
	//sets the root to the value
	public void setRoot(leaf<Food> l){
		
		root= l;
	}
	
	/**
	 * Checks tree if empty
	 *
	 * @return boolean
	 */
	//checks to see if the tree is empty
	public boolean isEmpty(){
		
		return root== null;
	}
	
	/**
	 * Empties tree
	 *
	 * @return void
	 */
	//empties the tree
	public void clear(){
		
		root=null;
	}
	
	/**
	 * Preorder
	 *
	 *@return void
	 */
	//prints out the tree in preorder
	public void preOrder(){
		
		preOrder(root);
	}
	
	/**
	 * Preorder
	 *
	 * @param leaf l
	 * @return void
	 */
	//prints the tree out in preorder
	public void preOrder(leaf<Food> t){
		
		 if(t!= null){
	        	
			 	System.out.println(t.getData().getName());
	            preOrder(t.left);
	            preOrder(t.right);
	        }
	}
	
	/**
	 * inOrder
	 *
	 * @return void
	 */
	//prints out the tree in inorder
	public void inOrder(){
		
		inOrder(root);
	}

	/**
	 * inOrder
	 *
	 * @param leaf l
	 * @return void
	 */
	//prints out the tree in inorder
	public void inOrder(leaf<Food> t){
		
		if(t!= null){
        	
            inOrder(t.left);
            System.out.println(t.getData().getName());
            inOrder(t.right);
		}
	}
	
	/**
	 * postOrder
	 *
	 * @return void
	 */
	//prints out the tree in postorder
	public void postOrder(){
		
		postOrder(root);
	}
	
	/**
	 * Postorder
	 *
	 * @param leaf l
	 * @return void
	 */
	//prints out the tree in postorder
	public void postOrder(leaf<Food> t){
		
		if(t!= null){
        	
            postOrder(t.left);
            postOrder(t.right);
            System.out.println(t.getData().getName());
        }
	}
	
	/**
	 * Height
	 *
	 * @param leaf l
	 * @return int
	 */
	//returns the height on the tree
	public int getHeight(leaf<Food> l){
		
		return l == null ? -1 : l.height;
	}
	
	//returns whichever value is max
	public int getMax(int l, int r){
		
		return l > r ? l : r;
	}

	//returns the value of the search 
	public boolean search(Food val){
		 
         return search(root, val);
    }
	
	//searches the tree if a food is in it
	public boolean search(leaf<Food> r, Food val){
		
        boolean found = false;
        while((r != null) && !found){
        	
            Food rval = r.getData();
            if(val.compareTo(rval) < 0){
             
            	r = r.left;
            }else if (val.compareTo(rval) >0){
               
            	r = r.right;
            }else if(val.getName()== rval.getName()){
            	
            	found=true;
            	break;
            }else{
            	
                found = true;
                break;
            }
            found = search(r, val);
        }
        return found;
    }

	//adds the food
	 public void add(Food data){
		 
         root = add(root, data);
     }
	 
	//adds the food to the tree
	public leaf<Food> add(leaf<Food> l, Food f){
		
		if(l==null){
			
			l=new leaf<Food>(f);
		}else if(f.compareTo(l.getData()) <0){
			
			l.left= add(l.left, f);
			if (getHeight(l.left) - getHeight(l.right)== 2){
               
				if (f.compareTo(l.left.getData()) <0){
                 
					l = rotateWithLeftChild(l);
				}else{
					
                    l= doubleWithLeftChild(l);
				}
			}
        }else if (f.compareTo(l.getData()) >0){
        	
            l.right = add(l.right, f);
            if(getHeight(l.right ) - getHeight(l.left) == 2)
                if (f.compareTo(l.right.getData()) > 0){
                 
                	l= rotateWithRightChild(l);
                }else{
                	
                    l= doubleWithRightChild(l);
                }
        }else{
        	
        	l.height = getMax(getHeight(l.left), getHeight(l.right)) + 1;
        	return l;
		}
		return l;
	}
	
	 /* Rotate binary tree node with left child */     
    @SuppressWarnings("unchecked")
	public leaf<Food> rotateWithLeftChild(leaf<Food> k2)
    {
        leaf<Food> k1= k2.left;
        k2.left= k1.right;
        k1.right= k2;
        k2.height= getMax(getHeight(k2.left), getHeight(k2.right))+ 1;
        k1.height = getMax(getHeight(k1.left), k2.height)+ 1;
        return k1;
    }

    /* Rotate binary tree node with right child */
    public leaf<Food> rotateWithRightChild(leaf<Food> k1)
    {
        leaf<Food> k2= k1.right;
        k1.right= k2.left;
        k2.left= k1;
        k1.height= getMax(getHeight(k1.left), getHeight(k1.right))+ 1;
        k2.height= getMax(getHeight(k2.right), k1.height) + 1;
        return k2;
    }
    
  //Rotates left child twice
    public leaf<Food> doubleWithLeftChild(leaf<Food> k3){
    	
        k3.left = rotateWithRightChild( k3.left );
        return rotateWithLeftChild( k3 );
    }
    
    //rotates the right child twice
    public leaf<Food> doubleWithRightChild(leaf<Food> k1){
    	
        k1.right = rotateWithLeftChild( k1.right );
        return rotateWithRightChild( k1 );
    }    
    
    //removes a food from the tree
	public boolean remove(Food d){
		
		if (root == null){
			
			return false;
		}else{
            if (root.getData() == d) {
                  
            	leaf<Food> auxRoot = new leaf<Food>(d);
                auxRoot.setLeft(root);
                
                boolean result = root.remove(d, auxRoot);
                root = auxRoot.getLeft();
                return result;
            } else {
            	
                  return root.remove(d, null);
            }
      }
	}
	
	//Prints the tree, testing only
	public void printTree(leaf<Food> t){
		
        if(t!= null){
        	
            printTree(t.left);
            System.out.println(t.getData().getName());
            printTree(t.right);
        }
    }	
}
