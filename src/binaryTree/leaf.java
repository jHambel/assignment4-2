package binaryTree;

import java.io.Serializable;

import linkedList.Node;
import src.Food;

//Node in each of our linked list
//Generic should be an object
@SuppressWarnings("serial")
public class leaf<T> implements Serializable{

	leaf<T> left;
	leaf<T> right;
	T data;
	int height;
	
	/**
	 * Constructor
	 *
	 * @param Data d
	 */
	//Constructor
	public leaf(T d){
		
		left=null;
		right=null;
		data=d;
		height=0;
	}
	
	/**
	 * Constructor
	 *
	 * @param Data d, leaf l, leaf r
	 */
	//Constructor used if I want to specify the next node
	public leaf(T d, leaf<T> l, leaf<T> r){
		
		left=l;
		right=r;
		data=d;
		height=0;
	}
	
	/**
	 * Returns data
	 *
	 * @return data
	 */
	//Returns the data
	public T getData(){
		
		return data;
	}
	
	/**
	 * Sets data
	 *
	 * @param data d
	 * @return void
	 */
	//Sets the data value to d
	public void setData(T d){
	
		data=d;
	}
	
	//Returns the left node
	public leaf<T> getLeft(){
		
		return left;
	}
	
	//Returns the right node
	public leaf<T> getRight(){
			
		return right;
	}
		
	//Sets the left node
	public void setLeft(leaf<T> l){
		
		left=l;
	}
	
	//Sets the right node
	public void setRight(leaf<T> r){
			
		right=r;
	}

	//Adds a food to the leaf
	public boolean add(Food f){
		
        if (f.compareTo((Food) this.getData())==0){
         
        	return false;
        }else if (f.compareTo((Food) this.getData()) <0){
        	   	
              if (left== null){
            	  
                    left= new leaf(f);
                    return true;
              } else
            	  
                    return left.add(f);
        }else if (f.compareTo((Food) this.getData()) >0){
        	
        	if (right== null){
            	  
                    right = new leaf(f);
                    return true;
              }else{
            	  
                    return right.add(f);
              }
        }
        return false;
  }
	
	//Removes a food from the leaf
	@SuppressWarnings("unchecked")
	public boolean remove(Food f, leaf<T> leaf) {
       
		if (f.compareTo((Food) this.getData()) <0){
             
			if(left!= null){
                   
				return left.remove(f, this);
			}else{
                    
				return false;
			}    
        }else if(f.compareTo((Food) this.getData()) > 0){
        	
              if (right != null){
            	  
            	  return right.remove(f, this);
              }else{
            	  
            	  return false;
              }      
        }else{
        	
              if(left != null && right != null){
            	  
                    this.data = (T) right.minValue();
                    right.remove((Food) this.data, this);
              }else if(leaf.left == this){
            	  
                    leaf.left = (left != null) ? left : right;
              } else if (leaf.right == this) {
            	  
                    leaf.right = (left != null) ? left : right;
              }
              return true;
        }
  }

	//FInds the min value
  public Food minValue() {
        if (left == null){
        
        	return (Food) this.getData();
        }else{
            
        	return left.minValue();
        }
  }
}
