package src;

import java.io.Serializable;

//This is the food/drink class. This class is called to make the items which will
//be inside the vending machine
public class Food implements Comparable<Food>, Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//The food class is made up of 3 attributes: name, nutrition, and price
	String name;
	String nutrition;
	double price;
	String category;
	
	//Constructor
	public Food(){
			
			name= "";
			nutrition= "";
			price=0;
			category="";
	}
	//Constructor
	public Food(String nm, String n, double p, String cat){
		
		name= nm;
		nutrition= n;
		price=p;
		category=cat;
	}
	
	//Constructor
	public Food(String nm, String n, double p){
		
		name= nm;
		nutrition= n;
		price=p;
		category=null;
	}
	//Returns the name of the selected food
	public String getName(){
		
		return name;
	}
	
	public void setName(String nm){
		
		name=nm;
	}
	//Returns the nutrition info of the selected food
	public String getNutrition(){
		
		return nutrition;
	}
	
	public void setNutrition(String n){
		
		nutrition=n;
	}
	//Returns the price of the selected food
	public double getPrice(){
		
		return price;
	}
	
	public void setPrice(Double ds){
		
		price=ds;
	}
	//Returns food's category
	public String getCategory(){
		
		return category;
	}

	//Sets food's category
	public void setCategory(String c){
		
		category= c;
	}
	//Takes in a selected food, then matches it to any existing food
	public Food matchFood(String food){
		
		Food temp= new Food();
		Food Empty= temp.getEmpty();
		Food doritos= temp.getDoritos();
		Food fritos= temp.getFritos();
		Food gold= temp.getGold();
		Food water= temp.getWater();
		Food burger= temp.getBurger();
		Food rock= temp.getRock();
		Food pikachu= temp.getPikachu();
		Food juice= temp.getJuice();
		Food cat= temp.getCat();
		Food fries= temp.getFries();
		
		if(Empty.getName().equals(food)){
			
			temp=Empty;
		}else if(doritos.getName().equals(food)){
			
			temp= doritos;
		}else if(fritos.getName().equals(food)){
			
			temp= fritos;
		}else if(gold.getName().equals(food)){
			
			temp= gold;
		}else if(water.getName().equals(food)){
			
			temp= water;
		}else if(burger.getName().equals(food)){
			
			temp= burger;
		}else if(rock.getName().equals(food)){
			
			temp= rock;
		}else if(pikachu.getName().equals(food)){
			
			temp= pikachu;
		}else if(juice.getName().equals(food)){
			
			temp= juice;
		}else if(cat.getName().equals(food)){
			
			temp= cat;
		}else if(fries.getName().equals(food)){
			
			temp= fries;
		}else{
			
			temp.name="A";
		}
		return temp;
	}
	//Creates food type empty(used to handle empty food spaces) and returns it
	//Empty goes against naming convention to differentiate from other food items (since
	//it's not actually a food)
	public Food getEmpty(){
		
		Food Empty= new Food();
		Empty.name= "Empty";
		Empty.nutrition= "N/A";
		Empty.price=0;
		Empty.category="Empty";
		
		return Empty;
	}
	
	//Creates food type doritos and returns it
	public Food getDoritos(){
			
			Food doritos= new Food();
			doritos.name= "Doritos";
			doritos.nutrition= "Nutrition Facts" + 
							   "\nServing Size 11 chips (28 g)" +
							   "\nPer Serving % Daily Value*" +
							   "\nCalories 140" +
							   "\nCalories from Fat 63" +
							   "\nTotal Fat 7g 11%" +
							   "\nSaturated Fat 1g 5%" +
							   "\nCholesterol 0mg 0%" +
							   "\nSodium 200mg 8%" +
							   "\nCarbohydrates 17g 6%" +
							   "\nDietary Fiber 1g 4%" +
							   "\nSugars 2g" +
							   "\nProtein 2g" +
							   "\nVitamin A 0% � Vitamin C 0%" +
							   "\nCalcium 4% � Iron 0%" +
							   "\n*Based on a 2000 calorie diet";
			doritos.price=1.5;
			doritos.category= "Snack";
			
			return doritos;
	}
	
	//Creates food type fritos and returns it
	public Food getFritos(){
					
		Food frito= new Food();
		frito.name= "Fritos";
		frito.nutrition= "Nutrition Facts" +
						 "\nServing Size 32 chips (28 g)" +
						 "\nPer Serving % Daily Value*" +
						 "\nCalories 160" +
						 "\nCalories from Fat 90" +
						 "\nTotal Fat 10g 15%" +
						 "\nSaturated Fat 1.5g 7%" +
						 "\nPolyunsaturated Fat 6g" +
						 "\nMonounsaturated Fat 2.5g" +
						 "\nCholesterol 0mg 0%" +
						 "\nSodium 170mg 7%" +
						 "\nCarbohydrates 15g 5%" +
						 "\nDietary Fiber 1g 4%" +
						 "\nSugars 0g" +
						 "\nProtein 2g" +
						 "\nVitamin A 0% � Vitamin C 0%" +
						 "\nCalcium 2% � Iron 0%" +
						 "\n*Based on a 2000 calorie diet";
		frito.price=1.75;
		frito.category="Snack";
		
		return frito;
	}
	
	//Creates "food" type Gold and returns it
	public Food getGold(){
		
		Food gold= new Food();
		gold.name= "Gold";
		gold.nutrition= "It's literally a block of gold. You probably shouldn't eat this...";
		gold.price=500;
		gold.category= "Dinner";
		
		return gold;
	}
	//Creates "food" type cat and returns it
	public Food getCat(){
			
			Food cat= new Food();
			cat.name= "Cat";
			cat.nutrition= "A pet cat! You probably should eat this...";
			cat.price=200;
			cat.category= "Lunch";
			
			return cat;
	}
	//Creates food type burger and returns it
	public Food getBurger(){
			
			Food burger= new Food();
			burger.name= "Burger";
			burger.nutrition= "Yum, yum, this is a tasty burger.";
			burger.price=10.5;
			burger.category= "Dinner";
			
			return burger;
	}
	//Creates food type water and returns it
	public Food getWater(){
			
			Food water= new Food();
			water.name= "Water";
			water.nutrition= "Good Old H2O";
			water.price=1.5;
			water.category= "Snack";
			
			return water;
	}
	//Creates food type fires and returns it
	public Food getFries(){
			
			Food fries= new Food();
			fries.name= "Fries";
			fries.nutrition= "Salty french fries";
			fries.price=3.5;
			fries.category= "Snack";
			
			return fries;
	}
	//Creates food type juice and returns it
	public Food getJuice(){
			
			Food juice= new Food();
			juice.name= "Juice";
			juice.nutrition= "Fruity Juice";
			juice.price=2.5;
			juice.category= "Snack";
			
			return juice;
	}
	//Creates food type rock and returns it
	public Food getRock(){
			
			Food rock= new Food();
			rock.name= "Rock";
			rock.nutrition= "Its a rock....";
			rock.price=20;
			rock.category= "Lunch";
			
			return rock;
	}
	//Pika pika?
	public Food getPikachu(){
			
			Food pikachu= new Food();
			pikachu.name= "Pikachu";
			pikachu.nutrition= "Pika Pika";
			pikachu.price=10.5;
			pikachu.category= "Lunch";
			
			return pikachu;
	}
	
	@Override
	public int compareTo(Food o) {
		
		 if((o.getName()).compareTo(this.getName()) > 0){
			 
			 return 1;
		 }
		 if(o.getName().compareTo(this.getName()) <0){
			 
			 return -1;
		 }else{
			 
			 return 0; 
		 }		   
	}
}