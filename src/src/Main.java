package src;

import interact.loadFile;
import interact.saveFile;

import java.util.Scanner;

import binaryTree.Tree;
import binaryTree.leaf;

public class Main {

	public static void main(String[] args){
		
		//All the items to run
		Tree t= new Tree();
		Food temp= new Food();
		Scanner scan= new Scanner(System.in);
		saveFile saver= new saveFile();
		loadFile loader= new loadFile();
		
		boolean run= true;
		
		//While loop to keep the program running
		while(run== true){

		System.out.println("What would you like to do?");
		System.out.println("1: Insert");
		System.out.println("2: Search");
		System.out.println("3: Remove");
		System.out.println("4: Quit");
		System.out.println("5: Save");
		System.out.println("6: Load");
		System.out.println("7: Preorder");
		System.out.println("8: Inorder");
		System.out.println("9: Postorder");
		int c= scan.nextInt();
		
		switch(c){
		
		case 1:
			
			System.out.println("Type the name of the food "
					+ "you would like to add");
		
			Food f= temp.matchFood(scan.next());
			if(f!= null){
				if(t.search(f)==true || temp.matchFood(f.getName()).getName()!= "A"){
				
					t.add(f);
				}else{
					
					System.out.println("Food type not found! Would you like to add it? (y or n)");
					if(scan.next().equals("y")){
						
						System.out.println("Name: ");
						String n= scan.next();
						System.out.println("Price: ");
						double p= scan.nextDouble();
						System.out.println("Info: ");
						String i= scan.next();
						
						Food a= new Food(n, i, p);
						t.add(a);
					}
				}
			}else{
				
				System.out.println("Food type not found! Would you like to add it? (y or n)");
				if(scan.next().equals("y")){
					
					System.out.println("Name: ");
					String n= scan.next();
					System.out.println("Price: ");
					double p= scan.nextDouble();
					System.out.println("Info: ");
					String i= scan.next();
					
					Food a= new Food(n, i, p);
					t.add(a);
				}
			}
			
			break;
		case 2:
			
			System.out.println("Enter Food to search");
			Food l= temp.matchFood(scan.next());
			if(l!= null){
				if(t.search(l)==true || temp.matchFood(l.getName()).getName()!= "A"){
					
					System.out.println("Search result : food type " + l.getName() + " has info: " + l.getNutrition());
				}else{
					
					System.out.println("Food type not found! Would you like to add it? (y or n)");
					if(scan.next().equals("y")){
						
						System.out.println("Name: ");
						String n= scan.next();
						System.out.println("Price: ");
						double p= scan.nextDouble();
						System.out.println("Info: ");
						String i= scan.next();
						
						Food a= new Food(n, i, p);
						t.add(a);
					}
				}
			}else{
				
				System.out.println("Food not found! Would you like to add it? (y or n)");
				if(scan.next()== "y"){
					
					System.out.println("Name: ");
					String n= scan.next();
					System.out.println("Price: ");
					double p= scan.nextDouble();
					System.out.println("Info: ");
					String i= scan.next();
					
					Food a= new Food(n, i, p);
					t.add(a);
				}
			}
			break;
		case 3:
		
			System.out.println("Type the name of the food "
					+ "you would like to remove");
		
			Food y= temp.matchFood(scan.next());
			
			 t.remove(y);
			break;
		case 4:
			
			System.out.println("Goodbye!");
			run=false;
			break;
		case 5:
			
			saver.save(t);
			break;
			
		case 6: 
			
			t= loader.load();
			break;
			
		case 7:
			
			t.preOrder();
			break;
		case 8:
			
			t.printTree(t.getRoot());
			break;
		case 9:
			
			t.postOrder();
			break;
			}
		}
	}
}
